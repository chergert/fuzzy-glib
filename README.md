# Fuzzy-GLib

This is a variant of my original fuzzy code that aims to write the index
to disk and then use a read-only mmap() to read the index back. The origianl
fuzzy index was meant to be mutable in memory, and will continue to serve
that purpose well.

This code, is instead, focused on generating search indexes up front, and then
reading them afterwards many types without the upfront generation cost. Also,
it allows the search index to be shared by multiple processes.


